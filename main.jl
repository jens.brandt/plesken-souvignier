
include("src/print.jl")
include("src/shortestVectors.jl")
include("src/automorphismGroup.jl")

include("examples/A2.jl")
gram = A2_gram_tez3OhNg

print("gram matrix:\n")
printMatrix(gram)
print("\n")
shortVectors = shortestVectors(gram)
print("short vectors (", length(shortVectors), " vectors):\n")
printVectorList(shortVectors)
print("\n")
res = pleskenSouvignier(gram, shortVectors)
print("automorphism group (by ", size(res,1), " elements):\n")
for item in res
	printMatrix(item)
	print("\n")
end
