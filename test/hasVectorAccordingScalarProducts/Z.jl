
include("../../examples/Z.jl")
include("../../src/auxiliaryFunctions.jl")

using Test

@testset "compare scalarproducts in Z" begin
	for i in 1:length(Z_hasVectorAccordingScalarProducts_testVectors_uiVoo1ei)
		@test hasVectorAccordingScalarProducts(Z_gram_uiVoo1ei, Z_hasVectorAccordingScalarProducts_testVectors_uiVoo1ei[i], Z_hasVectorAccordingScalarProducts_comparisonList_uiVoo1ei, Z_hasVectorAccordingScalarProducts_scalarProductsComparisonList_uiVoo1ei) == Z_hasVectorAccordingScalarProducts_results_uiVoo1ei[i]
  end
end
