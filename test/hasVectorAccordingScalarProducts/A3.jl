
include("../../examples/A3.jl")
include("../../src/auxiliaryFunctions.jl")

using Test

@testset "compare scalarproducts in A3" begin
	for i in 1:length(A3_hasVectorAccordingScalarProducts_testVectors_eeng6Coa)
		@test hasVectorAccordingScalarProducts(A3_gram_eeng6Coa, A3_hasVectorAccordingScalarProducts_testVectors_eeng6Coa[i], A3_hasVectorAccordingScalarProducts_comparisonList_eeng6Coa, A3_hasVectorAccordingScalarProducts_scalarProductsComparisonList_eeng6Coa) == A3_hasVectorAccordingScalarProducts_results_eeng6Coa[i]
  end
end
