
include("../../examples/A2xZ.jl")
include("../../src/auxiliaryFunctions.jl")

using Test

@testset "compare scalarproducts in A2xZ" begin
	for i in 1:length(A2xZ_hasVectorAccordingScalarProducts_testVectors_vik9Ooba)
		@test hasVectorAccordingScalarProducts(A2xZ_gram_vik9Ooba, A2xZ_hasVectorAccordingScalarProducts_testVectors_vik9Ooba[i], A2xZ_hasVectorAccordingScalarProducts_comparisonList_vik9Ooba, A2xZ_hasVectorAccordingScalarProducts_scalarProductsComparisonList_vik9Ooba) == A2xZ_hasVectorAccordingScalarProducts_results_vik9Ooba[i]
  end
end
