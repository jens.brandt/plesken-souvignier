
include("../../examples/automorphismgroup_C2_in_dim_20.jl")
include("../../src/auxiliaryFunctions.jl")

using Test

@testset "compare scalarproducts in automorphismgroup_C2_in_dim_20" begin
	@test Set(filter( v -> hasVectorAccordingScalarProducts(automorphismgroup_C2_in_dim_20_gram_zot0taSh, v, automorphismgroup_C2_in_dim_20_hasVectorAccordingScalarProducts_comparisonList_zot0taSh, automorphismgroup_C2_in_dim_20_hasVectorAccordingScalarProducts_scalarProductsComparisonList_zot0taSh), automorphismgroup_C2_in_dim_20_hasVectorAccordingScalarProducts_testVectors_zot0taSh)) == Set(automorphismgroup_C2_in_dim_20_hasVectorAccordingScalarProducts_results_zot0taSh)
end
