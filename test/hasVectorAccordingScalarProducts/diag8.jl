
include("../../examples/diag8.jl")
include("../../src/auxiliaryFunctions.jl")

using Test

@testset "compare scalarproducts in diag8" begin
	for i in 1:length(diag8_hasVectorAccordingScalarProducts_testVectors_Xaeb6oHa)
		@test hasVectorAccordingScalarProducts(diag8_gram_Xaeb6oHa, diag8_hasVectorAccordingScalarProducts_testVectors_Xaeb6oHa[i], diag8_hasVectorAccordingScalarProducts_comparisonList_Xaeb6oHa, diag8_hasVectorAccordingScalarProducts_scalarProductsComparisonList_Xaeb6oHa) == diag8_hasVectorAccordingScalarProducts_results_Xaeb6oHa[i]
  end
end
