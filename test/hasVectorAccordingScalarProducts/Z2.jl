
include("../../examples/Z2.jl")
include("../../src/auxiliaryFunctions.jl")

using Test

@testset "compare scalarproducts in Z2" begin
	for i in 1:length(Z2_hasVectorAccordingScalarProducts_testVectors_aa0ooBai)
		@test hasVectorAccordingScalarProducts(Z2_gram_aa0ooBai, Z2_hasVectorAccordingScalarProducts_testVectors_aa0ooBai[i], Z2_hasVectorAccordingScalarProducts_comparisonList_aa0ooBai, Z2_hasVectorAccordingScalarProducts_scalarProductsComparisonList_aa0ooBai) == Z2_hasVectorAccordingScalarProducts_results_aa0ooBai[i]
  end
end
