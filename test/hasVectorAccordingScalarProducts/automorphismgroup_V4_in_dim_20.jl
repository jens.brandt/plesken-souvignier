
include("../../examples/automorphismgroup_V4_in_dim_20.jl")
include("../../src/auxiliaryFunctions.jl")

using Test

@testset "compare scalarproducts in automorphismgroup_V4_in_dim_20" begin
	@test Set(filter( v -> hasVectorAccordingScalarProducts(automorphismgroup_V4_in_dim_20_gram_baegh9Ah, v, automorphismgroup_V4_in_dim_20_hasVectorAccordingScalarProducts_comparisonList_baegh9Ah, automorphismgroup_V4_in_dim_20_hasVectorAccordingScalarProducts_scalarProductsComparisonList_baegh9Ah), automorphismgroup_V4_in_dim_20_hasVectorAccordingScalarProducts_testVectors_baegh9Ah)) == Set(automorphismgroup_V4_in_dim_20_hasVectorAccordingScalarProducts_results_baegh9Ah)
end
