
include("../../examples/A2.jl")
include("../../src/auxiliaryFunctions.jl")

using Test

@testset "compare scalarproducts in A2" begin
	for i in 1:length(A2_hasVectorAccordingScalarProducts_testVectors_tez3OhNg)
		@test hasVectorAccordingScalarProducts(A2_gram_tez3OhNg, A2_hasVectorAccordingScalarProducts_testVectors_tez3OhNg[i], A2_hasVectorAccordingScalarProducts_comparisonList_tez3OhNg, A2_hasVectorAccordingScalarProducts_scalarProductsComparisonList_tez3OhNg) == A2_hasVectorAccordingScalarProducts_results_tez3OhNg[i]
  end
end
