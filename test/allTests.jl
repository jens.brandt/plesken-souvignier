
using Test

@testset "Plesken Souvignier" begin
  include("shortVectors/allTests.jl")
  include("hasVectorAccordingScalarProducts/allTests.jl")
  include("automorphismGroup/allTests.jl")
end
