
include("../../examples/Z2.jl")
include("../../src/automorphismGroup.jl")
include("auxiliaryFunctions.jl")

using Test

@testset "automorphism group of Z2" begin
	@test compareGeneratedGroup(pleskenSouvignier(Z2_gram_aa0ooBai, Z2_pleskenSouvignier_shortVectors_aa0ooBai), Z2_pleskenSouvignier_result_aa0ooBai)
	@test compareGeneratedGroup(pleskenSouvignier(Z2_gram_aa0ooBai, Z2_pleskenSouvignier_shortVectors_only_permutations_aa0ooBai), Z2_pleskenSouvignier_result_only_permutations_aa0ooBai)
	@test compareGeneratedGroup(pleskenSouvignier(Z2_gram_aa0ooBai, Z2_pleskenSouvignier_shortVectors_aa0ooBai, Z2_pleskenSouvignier_imagesOfBasis_fix_first_basis_vector_aa0ooBai), Z2_pleskenSouvignier_result_fix_first_basis_vector_aa0ooBai)
end
