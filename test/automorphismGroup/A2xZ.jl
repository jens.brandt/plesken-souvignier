
include("../../examples/A2xZ.jl")
include("../../src/automorphismGroup.jl")
include("auxiliaryFunctions.jl")

using Test

@testset "automorphism group of A2xZ" begin
	@test compareGeneratedGroup(pleskenSouvignier(A2xZ_gram_vik9Ooba, A2xZ_pleskenSouvignier_shortVectors_vik9Ooba), A2xZ_pleskenSouvignier_result_vik9Ooba)
	@test compareGeneratedGroup(pleskenSouvignier(A2xZ_gram_vik9Ooba, A2xZ_pleskenSouvignier_shortVectors_only_permutations_vik9Ooba), A2xZ_pleskenSouvignier_result_only_permutations_vik9Ooba)
	@test compareGeneratedGroup(pleskenSouvignier(A2xZ_gram_vik9Ooba, A2xZ_pleskenSouvignier_shortVectors_vik9Ooba, A2xZ_pleskenSouvignier_imagesOfBasis_fix_first_basis_vector_vik9Ooba), A2xZ_pleskenSouvignier_result_fix_first_basis_vector_vik9Ooba)
end
