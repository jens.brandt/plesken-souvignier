
include("../../examples/automorphismgroup_V4_in_dim_20.jl")
include("../../src/automorphismGroup.jl")
include("auxiliaryFunctions.jl")

using Test

@testset "automorphism group of automorphismgroup_V4_in_dim_20" begin
	@test compareGeneratedGroup(pleskenSouvignier(automorphismgroup_V4_in_dim_20_gram_baegh9Ah, automorphismgroup_V4_in_dim_20_pleskenSouvignier_shortVectors_baegh9Ah), automorphismgroup_V4_in_dim_20_pleskenSouvignier_result_baegh9Ah)
	@test generatesTrivialGroup(pleskenSouvignier(automorphismgroup_V4_in_dim_20_gram_baegh9Ah, automorphismgroup_V4_in_dim_20_pleskenSouvignier_shortVectors_only_permutations_baegh9Ah))
	@test compareGeneratedGroup(pleskenSouvignier(automorphismgroup_V4_in_dim_20_gram_baegh9Ah, automorphismgroup_V4_in_dim_20_pleskenSouvignier_shortVectors_baegh9Ah, automorphismgroup_V4_in_dim_20_pleskenSouvignier_imagesOfBasis_fix_first_basis_vector_baegh9Ah), automorphismgroup_V4_in_dim_20_pleskenSouvignier_result_fix_first_basis_vector_baegh9Ah)
end
