
include("../../examples/A3.jl")
include("../../src/automorphismGroup.jl")
include("auxiliaryFunctions.jl")

using Test

@testset "automorphism group of A3" begin
	@test compareGeneratedGroup(pleskenSouvignier(A3_gram_eeng6Coa, A3_pleskenSouvignier_shortVectors_eeng6Coa), A3_pleskenSouvignier_result_eeng6Coa)
	@test compareGeneratedGroup(pleskenSouvignier(A3_gram_eeng6Coa, A3_pleskenSouvignier_shortVectors_only_permutations_eeng6Coa), A3_pleskenSouvignier_result_only_permutations_eeng6Coa)
	@test compareGeneratedGroup(pleskenSouvignier(A3_gram_eeng6Coa, A3_pleskenSouvignier_shortVectors_eeng6Coa, A3_pleskenSouvignier_imagesOfBasis_fix_first_basis_vector_eeng6Coa), A3_pleskenSouvignier_result_fix_first_basis_vector_eeng6Coa)
end
