
include("../../examples/automorphismgroup_C2_in_dim_20.jl")
include("../../src/automorphismGroup.jl")
include("auxiliaryFunctions.jl")

using Test

@testset "automorphism group of automorphismgroup_C2_in_dim_20" begin
	@test compareGeneratedGroup(pleskenSouvignier(automorphismgroup_C2_in_dim_20_gram_zot0taSh, automorphismgroup_C2_in_dim_20_pleskenSouvignier_shortVectors_zot0taSh), automorphismgroup_C2_in_dim_20_pleskenSouvignier_result_zot0taSh)
	@test generatesTrivialGroup(pleskenSouvignier(automorphismgroup_C2_in_dim_20_gram_zot0taSh, automorphismgroup_C2_in_dim_20_pleskenSouvignier_shortVectors_only_permutations_zot0taSh))
	@test generatesTrivialGroup(pleskenSouvignier(automorphismgroup_C2_in_dim_20_gram_zot0taSh, automorphismgroup_C2_in_dim_20_pleskenSouvignier_shortVectors_zot0taSh, automorphismgroup_C2_in_dim_20_pleskenSouvignier_imagesOfBasis_fix_first_basis_vector_zot0taSh))
end
