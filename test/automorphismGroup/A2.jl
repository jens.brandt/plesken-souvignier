
include("../../examples/A2.jl")
include("../../src/automorphismGroup.jl")
include("auxiliaryFunctions.jl")

using Test

@testset "automorphism group of A2" begin
	@test compareGeneratedGroup(pleskenSouvignier(A2_gram_tez3OhNg, A2_pleskenSouvignier_shortVectors_tez3OhNg), A2_pleskenSouvignier_result_tez3OhNg)
	@test compareGeneratedGroup(pleskenSouvignier(A2_gram_tez3OhNg, A2_pleskenSouvignier_shortVectors_only_permutations_tez3OhNg), A2_pleskenSouvignier_result_only_permutations_tez3OhNg)
	@test compareGeneratedGroup(pleskenSouvignier(A2_gram_tez3OhNg, A2_pleskenSouvignier_shortVectors_tez3OhNg, A2_pleskenSouvignier_imagesOfBasis_fix_first_basis_vector_tez3OhNg), A2_pleskenSouvignier_result_fix_first_basis_vector_tez3OhNg)
end
