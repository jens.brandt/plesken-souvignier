
include("../../examples/diag8.jl")
include("../../src/automorphismGroup.jl")
include("auxiliaryFunctions.jl")

using Test

@testset "automorphism group of diag8" begin
	@test compareGeneratedGroup(pleskenSouvignier(diag8_gram_Xaeb6oHa, diag8_pleskenSouvignier_shortVectors_Xaeb6oHa), diag8_pleskenSouvignier_result_Xaeb6oHa)
	@test generatesTrivialGroup(pleskenSouvignier(diag8_gram_Xaeb6oHa, diag8_pleskenSouvignier_shortVectors_only_permutations_Xaeb6oHa))
	for i in length(diag8_pleskenSouvignier_imagesOfBasis_fix_first_basis_vectors_Xaeb6oHa)
		@test compareGeneratedGroup(pleskenSouvignier(diag8_gram_Xaeb6oHa, diag8_pleskenSouvignier_shortVectors_Xaeb6oHa, diag8_pleskenSouvignier_imagesOfBasis_fix_first_basis_vectors_Xaeb6oHa[i]), diag8_pleskenSouvignier_result_fix_first_basis_vectors_Xaeb6oHa[i])
	end
end
