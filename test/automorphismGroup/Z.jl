
include("../../examples/Z.jl")
include("../../src/automorphismGroup.jl")
include("auxiliaryFunctions.jl")

using Test

@testset "automorphism group of Z" begin
	@test compareGeneratedGroup(pleskenSouvignier(Z_gram_uiVoo1ei, Z_pleskenSouvignier_shortVectors_uiVoo1ei), Z_pleskenSouvignier_result_uiVoo1ei)
	@test generatesTrivialGroup(pleskenSouvignier(Z_gram_uiVoo1ei, Z_pleskenSouvignier_shortVectors_uiVoo1ei, Z_pleskenSouvignier_imagesOfBasis_fix_the_basis_vector_uiVoo1ei))
end
