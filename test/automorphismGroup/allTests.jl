
using Test

@testset "automorphism group" begin
  include("Z.jl")
  include("Z2.jl")
  include("A2.jl")
  include("A3.jl")
  include("A2xZ.jl")
  include("diag8.jl")
  include("automorphismgroup_C2_in_dim_20.jl")
  include("automorphismgroup_V4_in_dim_20.jl")
end
