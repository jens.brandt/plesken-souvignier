
using GAP
using LinearAlgebra

function generatedGroup(matrixList::Array{Array{Int64,2},1})::Main.ForeignGAP.MPtr
	matrixListGAP = GAP.julia_to_gap(map(M -> GAP.julia_to_gap(map(GAP.julia_to_gap, M)), matrixList))
	return GAP.Globals.Group(matrixListGAP)
end

function compareGeneratedGroup(matrixList1::Array{Array{Int64,2},1}, matrixList2::Array{Array{Int64,2},1})::Bool
	return generatedGroup(matrixList1) == generatedGroup(matrixList2)
end

function generatesTrivialGroup(matrixList::Array{Array{Int64,2},1})::Bool
	if isempty(matrixList)
		return true
	end
	n = size(matrixList[1], 1)
	return Set([ convert(Array{Int64,2}, Diagonal([1 for i in 1:n])) ]) == Set(matrixList)
end
