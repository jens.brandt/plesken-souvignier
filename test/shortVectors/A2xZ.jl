
include("../../examples/A2xZ.jl")
include("../../src/shortestVectors.jl")

using Test

@testset "shortVectors in A2xZ" begin
  @test isempty(shortestVectors(A2xZ_gram_vik9Ooba, 0))
  @test isempty(shortestVectors(A2xZ_gram_vik9Ooba, 1))
  @test Set(shortestVectors(A2xZ_gram_vik9Ooba)) == Set(A2xZ_shortestVectors_limit_2_3_vik9Ooba)
  @test Set(shortestVectors(A2xZ_gram_vik9Ooba, 2)) == Set(A2xZ_shortestVectors_limit_2_3_vik9Ooba)
  @test Set(shortestVectors(A2xZ_gram_vik9Ooba, 3)) == Set(A2xZ_shortestVectors_limit_2_3_vik9Ooba)
  @test Set(shortestVectors(A2xZ_gram_vik9Ooba, 4)) == Set(A2xZ_shortestVectors_limit_4_5_vik9Ooba)
  @test Set(shortestVectors(A2xZ_gram_vik9Ooba, 5)) == Set(A2xZ_shortestVectors_limit_4_5_vik9Ooba)
  @test Set(shortestVectors(A2xZ_gram_vik9Ooba, 6)) == Set(A2xZ_shortestVectors_limit_6_vik9Ooba)
end
