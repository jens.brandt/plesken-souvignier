
include("../../examples/automorphismgroup_V4_in_dim_20.jl")
include("../../src/shortestVectors.jl")

using Test

@testset "shortVectors in automorphismgroup_V4_in_dim_20" begin
  @test isempty(shortestVectors(automorphismgroup_V4_in_dim_20_gram_baegh9Ah, 0))
  @test isempty(shortestVectors(automorphismgroup_V4_in_dim_20_gram_baegh9Ah, 1))
  @test Set(shortestVectors(automorphismgroup_V4_in_dim_20_gram_baegh9Ah, 2)) == Set(automorphismgroup_V4_in_dim_20_shortestVectors_limit_2_baegh9Ah)
  @test Set(shortestVectors(automorphismgroup_V4_in_dim_20_gram_baegh9Ah, 3)) == Set(automorphismgroup_V4_in_dim_20_shortestVectors_limit_3_baegh9Ah)
  @test Set(shortestVectors(automorphismgroup_V4_in_dim_20_gram_baegh9Ah, 4)) == Set(automorphismgroup_V4_in_dim_20_shortestVectors_limit_4_baegh9Ah)
  @test Set(shortestVectors(automorphismgroup_V4_in_dim_20_gram_baegh9Ah)) == Set(automorphismgroup_V4_in_dim_20_shortestVectors_limit_21_baegh9Ah)
end
