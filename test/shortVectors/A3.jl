
include("../../examples/A3.jl")
include("../../src/shortestVectors.jl")

using Test

@testset "shortVectors in A3" begin
  @test isempty(shortestVectors(A3_gram_eeng6Coa, 0))
  @test isempty(shortestVectors(A3_gram_eeng6Coa, 1))
  @test Set(shortestVectors(A3_gram_eeng6Coa)) == Set(A3_shortestVectors_limit_2_3_eeng6Coa)
  @test Set(shortestVectors(A3_gram_eeng6Coa, 2)) == Set(A3_shortestVectors_limit_2_3_eeng6Coa)
  @test Set(shortestVectors(A3_gram_eeng6Coa, 3)) == Set(A3_shortestVectors_limit_2_3_eeng6Coa)
  @test Set(shortestVectors(A3_gram_eeng6Coa, 4)) == Set(A3_shortestVectors_limit_4_5_eeng6Coa)
  @test Set(shortestVectors(A3_gram_eeng6Coa, 5)) == Set(A3_shortestVectors_limit_4_5_eeng6Coa)
end
