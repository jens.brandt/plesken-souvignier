
include("../../examples/Z2.jl")
include("../../src/shortestVectors.jl")

using Test

@testset "shortVectors in Z2" begin
  @test isempty(shortestVectors(Z2_gram_aa0ooBai, 0))
  @test Set(shortestVectors(Z2_gram_aa0ooBai)) == Set(Z2_shortestVectors_limit_1_aa0ooBai)
  @test Set(shortestVectors(Z2_gram_aa0ooBai, 1)) == Set(Z2_shortestVectors_limit_1_aa0ooBai)
  @test Set(shortestVectors(Z2_gram_aa0ooBai, 2)) == Set(Z2_shortestVectors_limit_2_3_aa0ooBai)
  @test Set(shortestVectors(Z2_gram_aa0ooBai, 3)) == Set(Z2_shortestVectors_limit_2_3_aa0ooBai)
  @test Set(shortestVectors(Z2_gram_aa0ooBai, 4)) == Set(Z2_shortestVectors_limit_4_aa0ooBai)
  @test Set(shortestVectors(Z2_gram_aa0ooBai, 10)) == Set(Z2_shortestVectors_limit_10_aa0ooBai)
end
