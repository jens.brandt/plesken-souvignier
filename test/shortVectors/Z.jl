
include("../../examples/Z.jl")
include("../../src/shortestVectors.jl")

using Test

@testset "shortVectors in Z" begin
  @test isempty(shortestVectors(Z_gram_uiVoo1ei, 0))
	@test Set(shortestVectors(Z_gram_uiVoo1ei)) == Set(Z_shortestVectors_limit_1_uiVoo1ei)
	@test Set(shortestVectors(Z_gram_uiVoo1ei, 1*1)) == Set(Z_shortestVectors_limit_1_uiVoo1ei)
	@test Set(shortestVectors(Z_gram_uiVoo1ei, 2*2-1)) == Set(Z_shortestVectors_limit_1_uiVoo1ei)
	@test Set(shortestVectors(Z_gram_uiVoo1ei, 3*3)) == Set(Z_shortestVectors_limit_3_uiVoo1ei)
	@test Set(shortestVectors(Z_gram_uiVoo1ei, 4*4-1)) == Set(Z_shortestVectors_limit_3_uiVoo1ei)
	@test Set(shortestVectors(Z_gram_uiVoo1ei, 7*7)) == Set(Z_shortestVectors_limit_7_uiVoo1ei)
end
