
include("../../examples/A2.jl")
include("../../src/shortestVectors.jl")

using Test

@testset "shortVectors in A2" begin
  @test isempty(shortestVectors(A2_gram_tez3OhNg, 0))
  @test isempty(shortestVectors(A2_gram_tez3OhNg, 1))
  @test Set(shortestVectors(A2_gram_tez3OhNg)) == Set(A2_shortestVectors_limit_2_5_tez3OhNg)
  @test Set(shortestVectors(A2_gram_tez3OhNg, 2)) == Set(A2_shortestVectors_limit_2_5_tez3OhNg)
  @test Set(shortestVectors(A2_gram_tez3OhNg, 3)) == Set(A2_shortestVectors_limit_2_5_tez3OhNg)
  @test Set(shortestVectors(A2_gram_tez3OhNg, 4)) == Set(A2_shortestVectors_limit_2_5_tez3OhNg)
  @test Set(shortestVectors(A2_gram_tez3OhNg, 5)) == Set(A2_shortestVectors_limit_2_5_tez3OhNg)
  @test Set(shortestVectors(A2_gram_tez3OhNg, 6)) == Set(A2_shortestVectors_limit_6_tez3OhNg)
  @test Set(shortestVectors(A2_gram_tez3OhNg, 10)) == Set(A2_shortestVectors_limit_10_tez3OhNg)
end
