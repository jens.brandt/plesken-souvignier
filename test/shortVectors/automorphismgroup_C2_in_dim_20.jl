
include("../../examples/automorphismgroup_C2_in_dim_20.jl")
include("../../src/shortestVectors.jl")

using Test

@testset "shortVectors in automorphismgroup_C2_in_dim_20" begin
  @test isempty(shortestVectors(automorphismgroup_C2_in_dim_20_gram_zot0taSh, 0))
  @test isempty(shortestVectors(automorphismgroup_C2_in_dim_20_gram_zot0taSh, 1))
  @test isempty(shortestVectors(automorphismgroup_C2_in_dim_20_gram_zot0taSh, 2))
  @test Set(shortestVectors(automorphismgroup_C2_in_dim_20_gram_zot0taSh, 3)) == Set(automorphismgroup_C2_in_dim_20_shortestVectors_limit_3_zot0taSh)
  @test Set(shortestVectors(automorphismgroup_C2_in_dim_20_gram_zot0taSh)) == Set(automorphismgroup_C2_in_dim_20_shortestVectors_limit_22_zot0taSh)
end
