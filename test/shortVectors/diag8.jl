
include("../../examples/diag8.jl")
include("../../src/shortestVectors.jl")

using Test

@testset "shortVectors in diag8" begin
  @test isempty(shortestVectors(diag8_gram_Xaeb6oHa, 0))
  @test Set(shortestVectors(diag8_gram_Xaeb6oHa, 1)) == Set(diag8_shortestVectors_limit_1_Xaeb6oHa)
  @test Set(shortestVectors(diag8_gram_Xaeb6oHa, 2)) == Set(diag8_shortestVectors_limit_2_Xaeb6oHa)
  @test Set(shortestVectors(diag8_gram_Xaeb6oHa, 3)) == Set(diag8_shortestVectors_limit_3_Xaeb6oHa)
  @test Set(shortestVectors(diag8_gram_Xaeb6oHa)) == Set(diag8_shortestVectors_limit_8_Xaeb6oHa)
end
