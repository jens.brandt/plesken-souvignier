
using LinearAlgebra
using GAP

function shortestVectors(gramMatrix::Array{Int64,2}, limitNormSquare::Int64 = -1)::Array{Array{Int64,1},1}
	@assert(size(gramMatrix,1) == size(gramMatrix,2))
	if limitNormSquare < 0
		limitNormSquare = maximum(Diagonal(gramMatrix))
	end
	gramMatrixGAP = GAP.julia_to_gap(map(GAP.julia_to_gap, gramMatrix))
	recGAP = GAP.Globals.ShortestVectors(gramMatrixGAP, limitNormSquare)
	vectors = GAP.gap_to_julia(Array{Array{Int64,1},1}, recGAP.vectors)
	return vcat(map( v -> [v,-v], vectors)...)
end
