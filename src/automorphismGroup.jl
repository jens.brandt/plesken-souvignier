
include("auxiliaryFunctions.jl")

function pleskenSouvignier(gramMatrix::Array{Int64,2}, shortVectors::Array{Array{Int64,1},1}, imagesOfBasis::Array{Array{Int64,1},1} = convert(Array{Array{Int64,1},1}, []))::Array{Array{Int64,2},1}
	@assert(size(gramMatrix,1) == size(gramMatrix,2))
	for v in shortVectors
		@assert(size(gramMatrix,1) == length(v))
	end
	@assert(length(imagesOfBasis) <= size(gramMatrix,1))
	for v in imagesOfBasis
		@assert(size(gramMatrix,1) == length(v))
	end
	n = length(imagesOfBasis) + 1
	if n > size(gramMatrix,1)
		return [ reshape(hcat(imagesOfBasis...), (length(imagesOfBasis[1]), length(imagesOfBasis))) ]
	end
	imageCandidates = shortVectors
	# determine all vectors of the correct norm
	imageCandidates = filter( v -> scalarProduct(gramMatrix, v) == gramMatrix[n,n], imageCandidates)
	# determine all vectors of correct scalar products with given images
	imageCandidates = filter( v -> hasVectorAccordingScalarProducts(gramMatrix, v, imagesOfBasis, gramMatrix[n,1:(n-1)]), imageCandidates)
	res = []
	for possibleImage in imageCandidates
		nextImagesOfBasis = vcat(imagesOfBasis, [possibleImage])
		res = vcat(res, pleskenSouvignier(gramMatrix, shortVectors, nextImagesOfBasis))
	end
	return res
end
