
function printMatrix(M::Array{Int64,2}, indent::Int64 = 0)
	for i in 1:size(M, 1)
		for j in 1:indent
			print(" ")
		end
		println(M[i,:])
	end
end

function printVectorList(vectorList::Array{Array{Int64,1},1}, indent::Int64 = 0)
	asMatrix = permutedims(reshape(hcat(vectorList...), (length(vectorList[1]), length(vectorList))))
	printMatrix(asMatrix, indent)
end
