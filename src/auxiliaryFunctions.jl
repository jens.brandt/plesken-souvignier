
function scalarProduct(gramMatrix::Array{Int64,2}, vector1::Array{Int64,1}, vector2::Array{Int64,1} = vector1)::Int64
	@assert(size(gramMatrix,1) == length(vector1))
	@assert(size(gramMatrix,2) == length(vector2))
	return transpose(vector1) * gramMatrix * vector2
end

function hasVectorAccordingScalarProducts(gramMatrix::Array{Int64,2}, vector::Array{Int64,1}, vectorComparisonList::Array{Array{Int64,1},1}, scalarProductsComparisonList::Array{Int64,1})::Bool
	n = length(vectorComparisonList)
	@assert(size(gramMatrix,1) == size(gramMatrix,2))
	@assert(size(gramMatrix,1) == length(vector))
	for v in vectorComparisonList
		@assert(size(gramMatrix,1) == length(v))
	end
	@assert(length(scalarProductsComparisonList) == n)
	for i in 1:n
		if scalarProduct(gramMatrix, vector, vectorComparisonList[i]) != scalarProductsComparisonList[i]
			return false
		end
	end
	return true
end
