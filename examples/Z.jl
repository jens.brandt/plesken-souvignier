
using LinearAlgebra

### gram matrix

Z_gram_uiVoo1ei = convert(Array{Int64,2}, Diagonal([1]))

### shortestVectors

# limit norm square: 1
Z_shortestVectors_limit_1_uiVoo1ei = vcat(map( j -> [[j],[-j]], 1:1)...)

# limit norm square: 3
Z_shortestVectors_limit_3_uiVoo1ei = vcat(map( j -> [[j],[-j]], 1:3)...)

# limit norm square: 7
Z_shortestVectors_limit_7_uiVoo1ei = vcat(map( j -> [[j],[-j]], 1:7)...)

### hasVectorAccordingScalarProducts

Z_hasVectorAccordingScalarProducts_comparisonList_uiVoo1ei = [ [1] ]
Z_hasVectorAccordingScalarProducts_scalarProductsComparisonList_uiVoo1ei = [ -1 ]
Z_hasVectorAccordingScalarProducts_testVectors_uiVoo1ei = [ [-1], [0], [1] ]
Z_hasVectorAccordingScalarProducts_results_uiVoo1ei = [ true, false, false ]

### automorphism group

# complete automorphism group
Z_pleskenSouvignier_shortVectors_uiVoo1ei = [ [1], [-1] ]
Z_pleskenSouvignier_result_uiVoo1ei = [ convert(Array{Int64,2}, Diagonal([-1])) ]

# fix the basis vector
Z_pleskenSouvignier_imagesOfBasis_fix_the_basis_vector_uiVoo1ei = [ [1] ]
