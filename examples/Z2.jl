
using LinearAlgebra

### gram matrix

Z2_gram_aa0ooBai = convert(Array{Int64,2}, Diagonal([1, 1]))

### shortestVectors

# limit norm square: 1
Z2_shortestVectors_limit_1_pos_aa0ooBai = [ [1, 0], [0, 1] ]
Z2_shortestVectors_limit_1_aa0ooBai = vcat(map( v -> [v,-v], Z2_shortestVectors_limit_1_pos_aa0ooBai)...)

# limit norm square: 2-3
Z2_shortestVectors_limit_2_3_pos_aa0ooBai = vcat(Z2_shortestVectors_limit_1_pos_aa0ooBai, [ [1, 1], [1, -1] ])
Z2_shortestVectors_limit_2_3_aa0ooBai = vcat(map( v -> [v,-v], Z2_shortestVectors_limit_2_3_pos_aa0ooBai)...)

# limit norm square: 4
Z2_shortestVectors_limit_4_pos_aa0ooBai = vcat(Z2_shortestVectors_limit_2_3_pos_aa0ooBai, [ [2, 0], [0, 2] ])
Z2_shortestVectors_limit_4_aa0ooBai = vcat(map( v -> [v,-v], Z2_shortestVectors_limit_4_pos_aa0ooBai)...)

# limit norm square: 10
Z2_shortestVectors_limit_10_pos_aa0ooBai = [ [x, y] for x=0:3 for y=-3:3 if 0 < x*x+y*y <= 10 ]
Z2_shortestVectors_limit_10_aa0ooBai = vcat(map( v -> [v,-v], Z2_shortestVectors_limit_10_pos_aa0ooBai)...)

### hasVectorAccordingScalarProducts

Z2_hasVectorAccordingScalarProducts_comparisonList_aa0ooBai = [ [1, 0], [-2, 0] ]
Z2_hasVectorAccordingScalarProducts_scalarProductsComparisonList_aa0ooBai = [ -1, 2 ]
Z2_hasVectorAccordingScalarProducts_testVectors_aa0ooBai = Z2_shortestVectors_limit_1_aa0ooBai
Z2_hasVectorAccordingScalarProducts_results_aa0ooBai = [ false, true, false, false ]

### automorphism group

# complete automorphism group
Z2_pleskenSouvignier_shortVectors_aa0ooBai = Z2_shortestVectors_limit_1_aa0ooBai
Z2_pleskenSouvignier_result_aa0ooBai = [ [[0 1]; [1 0]], [[-1 0]; [0 1]] ]

# only permutations
Z2_pleskenSouvignier_shortVectors_only_permutations_aa0ooBai = [ [1, 0], [0, 1] ]
Z2_pleskenSouvignier_result_only_permutations_aa0ooBai = [ [[0 1]; [1 0]] ]

# fix first basis vector
Z2_pleskenSouvignier_imagesOfBasis_fix_first_basis_vector_aa0ooBai = [ [1, 0] ]
Z2_pleskenSouvignier_result_fix_first_basis_vector_aa0ooBai = [ [[1 0]; [0 -1]] ]
