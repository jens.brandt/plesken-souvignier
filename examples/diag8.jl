
using LinearAlgebra

### gram matrix

diag8_gram_Xaeb6oHa = convert(Array{Int64,2}, Diagonal([ i for i in 1:8 ]))

### shortestVectors

# limit norm square: 1
diag8_shortestVectors_limit_1_pos_Xaeb6oHa = [ [1, 0, 0, 0, 0, 0, 0, 0] ]
diag8_shortestVectors_limit_1_Xaeb6oHa = vcat(map( v -> [v,-v], diag8_shortestVectors_limit_1_pos_Xaeb6oHa)...)

# limit norm square: 2
diag8_shortestVectors_limit_2_pos_Xaeb6oHa = vcat(diag8_shortestVectors_limit_1_pos_Xaeb6oHa, [ [0, 1, 0, 0, 0, 0, 0, 0] ])
diag8_shortestVectors_limit_2_Xaeb6oHa = vcat(map( v -> [v,-v], diag8_shortestVectors_limit_2_pos_Xaeb6oHa)...)

# limit norm square: 3
diag8_shortestVectors_limit_3_pos_Xaeb6oHa = vcat(diag8_shortestVectors_limit_2_pos_Xaeb6oHa, [ [0, 0, 1, 0, 0, 0, 0, 0], [1, 1, 0, 0, 0, 0, 0, 0], [1, -1, 0, 0, 0, 0, 0, 0] ])
diag8_shortestVectors_limit_3_Xaeb6oHa = vcat(map( v -> [v,-v], diag8_shortestVectors_limit_3_pos_Xaeb6oHa)...)

# limit norm square: 8
diag8_shortestVectors_limit_8_pos_Xaeb6oHa = [ [0, 0, 0, 0, 0, 0, 0, 1], [1, 0, 0, 0, 0, 0, 1, 0], 
[0, 0, 0, 0, 0, 0, 1, 0], [-1, 0, 0, 0, 0, 0, 1, 0], [0, 1, 0, 0, 0, 1, 0, 0], [1, 0, 0, 0, 0, 1, 0, 0], 
[0, 0, 0, 0, 0, 1, 0, 0], [-1, 0, 0, 0, 0, 1, 0, 0], [0, -1, 0, 0, 0, 1, 0, 0], [0, 0, 1, 0, 1, 0, 0, 0], 
[1, 1, 0, 0, 1, 0, 0, 0], [0, 1, 0, 0, 1, 0, 0, 0], [-1, 1, 0, 0, 1, 0, 0, 0], [1, 0, 0, 0, 1, 0, 0, 0], 
[0, 0, 0, 0, 1, 0, 0, 0], [-1, 0, 0, 0, 1, 0, 0, 0], [1, -1, 0, 0, 1, 0, 0, 0], [0, -1, 0, 0, 1, 0, 0, 0], 
[-1, -1, 0, 0, 1, 0, 0, 0], [0, 0, -1, 0, 1, 0, 0, 0], [1, 0, 1, 1, 0, 0, 0, 0], [0, 0, 1, 1, 0, 0, 0, 0], 
[-1, 0, 1, 1, 0, 0, 0, 0], [1, 1, 0, 1, 0, 0, 0, 0], [0, 1, 0, 1, 0, 0, 0, 0], [-1, 1, 0, 1, 0, 0, 0, 0], 
[2, 0, 0, 1, 0, 0, 0, 0], [1, 0, 0, 1, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0, 0], [-1, 0, 0, 1, 0, 0, 0, 0], 
[-2, 0, 0, 1, 0, 0, 0, 0], [1, -1, 0, 1, 0, 0, 0, 0], [0, -1, 0, 1, 0, 0, 0, 0], [-1, -1, 0, 1, 0, 0, 0, 0], 
[1, 0, -1, 1, 0, 0, 0, 0], [0, 0, -1, 1, 0, 0, 0, 0], [-1, 0, -1, 1, 0, 0, 0, 0], [1, 1, 1, 0, 0, 0, 0, 0], 
[0, 1, 1, 0, 0, 0, 0, 0], [-1, 1, 1, 0, 0, 0, 0, 0], [2, 0, 1, 0, 0, 0, 0, 0], [1, 0, 1, 0, 0, 0, 0, 0], 
[0, 0, 1, 0, 0, 0, 0, 0], [-1, 0, 1, 0, 0, 0, 0, 0], [-2, 0, 1, 0, 0, 0, 0, 0], [1, -1, 1, 0, 0, 0, 0, 0], 
[0, -1, 1, 0, 0, 0, 0, 0], [-1, -1, 1, 0, 0, 0, 0, 0], [0, 2, 0, 0, 0, 0, 0, 0], [2, 1, 0, 0, 0, 0, 0, 0], 
[1, 1, 0, 0, 0, 0, 0, 0], [0, 1, 0, 0, 0, 0, 0, 0], [-1, 1, 0, 0, 0, 0, 0, 0], [-2, 1, 0, 0, 0, 0, 0, 0], 
[2, 0, 0, 0, 0, 0, 0, 0], [1, 0, 0, 0, 0, 0, 0, 0] ]
diag8_shortestVectors_limit_8_Xaeb6oHa = vcat(map( v -> [v,-v], diag8_shortestVectors_limit_8_pos_Xaeb6oHa)...)

### hasVectorAccordingScalarProducts

diag8_hasVectorAccordingScalarProducts_comparisonList_Xaeb6oHa = [ [-1, 1, 0, 0, 1, 0, 0, 0], [0, 0, 1, 0, 1, 0, 0, 0], [2, 0, 0, 1, 0, 0, 0, 0] ]
diag8_hasVectorAccordingScalarProducts_scalarProductsComparisonList_Xaeb6oHa = [ 2, 0, 0 ]
diag8_hasVectorAccordingScalarProducts_testVectors_Xaeb6oHa = diag8_shortestVectors_limit_8_Xaeb6oHa
diag8_hasVectorAccordingScalarProducts_results_Xaeb6oHa = map(v -> (-v[1]+2*v[2]+5*v[5] == 2) && (3*v[3]+5*v[5] == 0) && (2*v[1]+4*v[4] == 0), diag8_hasVectorAccordingScalarProducts_testVectors_Xaeb6oHa)

### automorphism group

# complete automorphism group
diag8_pleskenSouvignier_shortVectors_Xaeb6oHa = diag8_shortestVectors_limit_8_Xaeb6oHa
diag8_pleskenSouvignier_result_Xaeb6oHa = [ convert(Array{Int64,2}, Diagonal([ if i==j 0-1 else 1 end for j in 1:8 ])) for i in 1:8 ]

# only permutations
diag8_pleskenSouvignier_shortVectors_only_permutations_Xaeb6oHa = [ [ if i==j 1 else 0 end for j in 1:8 ] for i in 1:8 ] 

# fix first i = 1..7 basis vectors
diag8_pleskenSouvignier_imagesOfBasis_fix_first_basis_vectors_Xaeb6oHa = [ [ [ if k==j 1 else 0 end for k in 1:8 ] for j in 1:i ] for i in 1:7 ]
diag8_pleskenSouvignier_result_fix_first_basis_vectors_Xaeb6oHa = [ [ convert(Array{Int64,2}, Diagonal([ if k==j 0-1 else 1 end for k in 1:8 ])) for j in (i+1):8 ] for i in 1:7 ]
