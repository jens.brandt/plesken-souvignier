
function generateGrammatrixOfAn(n::Int64)::Array{Int64,2}
	@assert(n > 0)
	gram = zeros(Int64, n, n)
	for i in 1:n
		gram[i,i] = 2
		if i > 1
			gram[i,i-1] = -1
		end
		if i < n
			gram[i,i+1] = -1
		end
	end
	return gram
end
