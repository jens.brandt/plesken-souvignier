
include("auxiliaryFunctions.jl")

### gram matrix

A2xZ_gram_vik9Ooba = [[generateGrammatrixOfAn(2) zeros(Int64, 2, 1)];[zeros(Int64, 1, 2) 2]]

### shortestVectors

# limit norm square: 2-3
A2xZ_shortestVectors_limit_2_3_pos_vik9Ooba = [ [1, 0, 0], [0, 1, 0], [1, 1, 0], [0, 0, 1] ]
A2xZ_shortestVectors_limit_2_3_vik9Ooba = vcat(map( v -> [v,-v], A2xZ_shortestVectors_limit_2_3_pos_vik9Ooba)...)

# limit norm square: 4-5
A2xZ_shortestVectors_limit_4_5_pos_vik9Ooba = vcat(A2xZ_shortestVectors_limit_2_3_pos_vik9Ooba, [ [ 1, 1, 1 ], [ 0, 1, 1 ], [ 1, 0, 1 ], [ 1, 1, -1 ], [ 0, 1, -1 ], [ 1, 0, -1 ] ])
A2xZ_shortestVectors_limit_4_5_vik9Ooba = vcat(map( v -> [v,-v], A2xZ_shortestVectors_limit_4_5_pos_vik9Ooba)...)

# limit norm square: 6
A2xZ_shortestVectors_limit_6_pos_vik9Ooba = vcat(A2xZ_shortestVectors_limit_4_5_pos_vik9Ooba, [ [ 1, 2, 0 ], [ 2, 1, 0 ], [1, -1, 0] ])
A2xZ_shortestVectors_limit_6_vik9Ooba = vcat(map( v -> [v,-v], A2xZ_shortestVectors_limit_6_pos_vik9Ooba)...)

### hasVectorAccordingScalarProducts

A2xZ_hasVectorAccordingScalarProducts_comparisonList_vik9Ooba = [ [1, -1, 0] ]
A2xZ_hasVectorAccordingScalarProducts_scalarProductsComparisonList_vik9Ooba = [ -3 ]
A2xZ_hasVectorAccordingScalarProducts_testVectors_vik9Ooba = A2xZ_shortestVectors_limit_2_3_vik9Ooba
A2xZ_hasVectorAccordingScalarProducts_results_vik9Ooba = [ false, true, true, false, false, false, false, false ]

### automorphism group

# complete automorphism group
A2xZ_pleskenSouvignier_shortVectors_vik9Ooba = A2xZ_shortestVectors_limit_2_3_vik9Ooba
A2xZ_pleskenSouvignier_result_vik9Ooba = [ [[0 1 0]; [1 0 0]; [0 0 1]], [[1 0 0]; [1 -1 0]; [0 0 1]], [[1 0 0]; [0 1 0]; [0 0 -1]] ]

# only permutations
A2xZ_pleskenSouvignier_shortVectors_only_permutations_vik9Ooba = [ [1, 0, 0], [0, 1, 0], [0, 0, 1] ]
A2xZ_pleskenSouvignier_result_only_permutations_vik9Ooba = [ [[0 1 0]; [1 0 0]; [0 0 1]] ]

# fix first basis vector
A2xZ_pleskenSouvignier_imagesOfBasis_fix_first_basis_vector_vik9Ooba = [ [1, 0, 0] ]
A2xZ_pleskenSouvignier_result_fix_first_basis_vector_vik9Ooba = [ [[1 -1 0]; [0 -1 0]; [0 0 1]], [[1 0 0]; [0 1 0]; [0 0 -1]] ]
