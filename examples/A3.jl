
include("auxiliaryFunctions.jl")

### gram matrix

A3_gram_eeng6Coa = generateGrammatrixOfAn(3)

### shortestVectors

# limit norm square: 2-3
A3_shortestVectors_limit_2_3_pos_eeng6Coa = [ [1, 1, 1], [0, 1, 1], [0, 0, 1], [1, 1, 0], [0, 1, 0], [1, 0, 0] ]
A3_shortestVectors_limit_2_3_eeng6Coa = vcat(map( v -> [v,-v], A3_shortestVectors_limit_2_3_pos_eeng6Coa)...)

# limit norm square: 4-5
A3_shortestVectors_limit_4_5_pos_eeng6Coa = vcat(A3_shortestVectors_limit_2_3_pos_eeng6Coa, [ [1, 2, 1], [1, 0, 1], [1, 0, -1] ])
A3_shortestVectors_limit_4_5_eeng6Coa = vcat(map( v -> [v,-v], A3_shortestVectors_limit_4_5_pos_eeng6Coa)...)

### hasVectorAccordingScalarProducts

A3_hasVectorAccordingScalarProducts_comparisonList_eeng6Coa = [ [1, 1, 0], [1, 0, 1] ]
A3_hasVectorAccordingScalarProducts_scalarProductsComparisonList_eeng6Coa = [ -1, 2 ]
A3_hasVectorAccordingScalarProducts_testVectors_eeng6Coa = A3_shortestVectors_limit_2_3_eeng6Coa
A3_hasVectorAccordingScalarProducts_results_eeng6Coa = map(v -> (v[1] == 0) && (v[2]-v[3] == -1), A3_hasVectorAccordingScalarProducts_testVectors_eeng6Coa)

### automorphism group

# complete automorphism group
A3_pleskenSouvignier_shortVectors_eeng6Coa = A3_shortestVectors_limit_2_3_eeng6Coa
A3_pleskenSouvignier_result_eeng6Coa = [ [[0 0 1]; [0 1 0]; [1 0 0]], [[-1 0 0]; [-1 0 1]; [0 -1 1]], [[1 0 0]; [0 1 0]; [0 1 -1]] ]

# only permutations
A3_pleskenSouvignier_shortVectors_only_permutations_eeng6Coa = [ [1, 0, 0], [0, 1, 0], [0, 0, 1] ]
A3_pleskenSouvignier_result_only_permutations_eeng6Coa = [ [[0 0 1]; [0 1 0]; [1 0 0]] ]

# fix first basis vector
A3_pleskenSouvignier_imagesOfBasis_fix_first_basis_vector_eeng6Coa = [ [1, 0, 0] ]
A3_pleskenSouvignier_result_fix_first_basis_vector_eeng6Coa = [ [[1 0 0]; [0 1 0]; [0 1 -1]], [[1 -1 0]; [0 -1 0]; [0 -1 1]] ]
