
include("auxiliaryFunctions.jl")

### gram matrix

A2_gram_tez3OhNg = generateGrammatrixOfAn(2)

### shortestVectors

# limit norm square: 2-5
A2_shortestVectors_limit_2_5_pos_tez3OhNg = [ [1, 0], [0, 1], [1, 1] ]
A2_shortestVectors_limit_2_5_tez3OhNg = vcat(map( v -> [v,-v], A2_shortestVectors_limit_2_5_pos_tez3OhNg)...)

# limit norm square: 6
A2_shortestVectors_limit_6_pos_tez3OhNg = vcat(A2_shortestVectors_limit_2_5_pos_tez3OhNg, [ [1, 2], [2, 1], [1, -1] ])
A2_shortestVectors_limit_6_tez3OhNg = vcat(map( v -> [v,-v], A2_shortestVectors_limit_6_pos_tez3OhNg)...)

# limit norm square: 10
A2_shortestVectors_limit_10_pos_tez3OhNg = vcat(A2_shortestVectors_limit_6_pos_tez3OhNg, [ [2, 0], [0, 2], [2, 2] ])
A2_shortestVectors_limit_10_tez3OhNg = vcat(map( v -> [v,-v], A2_shortestVectors_limit_10_pos_tez3OhNg)...)

### hasVectorAccordingScalarProducts

A2_hasVectorAccordingScalarProducts_comparisonList_tez3OhNg = [ [1, 1] ]
A2_hasVectorAccordingScalarProducts_scalarProductsComparisonList_tez3OhNg = [ -1 ]
A2_hasVectorAccordingScalarProducts_testVectors_tez3OhNg = A2_shortestVectors_limit_2_5_tez3OhNg
A2_hasVectorAccordingScalarProducts_results_tez3OhNg = [ false, true, false, true, false, false ]

### automorphism group

# complete automorphism group
A2_pleskenSouvignier_shortVectors_tez3OhNg = A2_shortestVectors_limit_2_5_tez3OhNg
A2_pleskenSouvignier_result_tez3OhNg = [ [[0 1]; [1 0]], [[1 0]; [1 -1]] ]

# only permutations
A2_pleskenSouvignier_shortVectors_only_permutations_tez3OhNg = [ [1, 0], [0, 1] ]
A2_pleskenSouvignier_result_only_permutations_tez3OhNg = [ [[0 1]; [1 0]] ]

# fix first basis vector
A2_pleskenSouvignier_imagesOfBasis_fix_first_basis_vector_tez3OhNg = [ [1, 0] ]
A2_pleskenSouvignier_result_fix_first_basis_vector_tez3OhNg = [ [[1 -1]; [0 -1]] ]
